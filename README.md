### Project Information

This application allows you to create wallets with currencies you select, and make transfers to the other users  
(if they have at least one wallet of the same currency).

To do this operations you need to be authorized, for that register first.

### How it works
The system uses database context, writes and fetches data about users, wallets and currency amounts from database.