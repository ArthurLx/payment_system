﻿using System.Collections.Generic;

namespace BlazorApp.Shared
{
    public class CurrencyList
    {
        public List<string> Currencies { get; set; }
    }
}
