﻿using System;

namespace BlazorApp.Shared
{
    public class Wallet
    {
        public Guid ID { get; set; }
        public decimal Amount { get; set; }
        public String Currency { get; set; }
    }
}