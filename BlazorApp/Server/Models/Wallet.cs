﻿using System;

namespace BlazorApp.Server.Models
{
    public class Wallet
    {
        public Guid ID { get; set; }
        public decimal Amount { get; set; }
        public String Currency { get; set; }
        public string ApplicationUserId { get; set; }
    }
}
