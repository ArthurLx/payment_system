﻿using System.Collections.Generic;
using BlazorApp.Shared;
using Microsoft.AspNetCore.Mvc;

namespace BlazorApp.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrencyController : ControllerBase
    {
        [HttpGet]
        public CurrencyList GetCurrencies()
        {
            return new CurrencyList
            {
                Currencies = new List<string>
                {
                    "USD",
                    "EUR",
                    "MDL",
                    "RUB",
                    "UAH"
                }
            };
        }
    }
}
